#include <stdlib.h>
#include <linearsolver.h>

int SIPCleanUp(void *s)
{
  SIP *sip = (SIP*) s;

  /* these are allocated in SIPInitialize() */
  free(sip->Ac);
  free(sip->Ae);
  free(sip->Aw);
  free(sip->At);
  free(sip->Ab);
  free(sip->An);
  free(sip->As);
  free(sip->Lc);
  free(sip->Lw);
  free(sip->Lb);
  free(sip->Ls);
  free(sip->Un);
  free(sip->Ue);
  free(sip->Ut);

  return(0);
}
