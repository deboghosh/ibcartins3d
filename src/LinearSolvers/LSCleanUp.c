#include <stdlib.h>
#include <linearsolver.h>

int LSCleanUp(void *ls)
{
  LinearSolver *LS = (LinearSolver*) ls;

  SIPCleanUp(LS->scheme);
  /* this is allocated in LSInitialize() */
  free(LS->scheme);

  return(0);
}
