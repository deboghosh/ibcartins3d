#include <stdlib.h>
#include <linearsolver.h>

int SIPInitialize(void *s,int imax,int jmax,int kmax)
{
  SIP *sip = (SIP*) s;

  sip->alpha = 0.9;

  sip->Ac = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->Ae = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->Aw = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->An = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->As = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->At = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->Ab = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->Lc = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->Lw = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->Ls = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->Lb = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->Ut = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->Un = (double*) calloc (imax*jmax*kmax,sizeof(double));
  sip->Ue = (double*) calloc (imax*jmax*kmax,sizeof(double));

  return(0);
}
