#include <arrayfunctions.h>
#include <mathroutines.h>
#include <mpivars.h>
#include <ibcartins3d.h>

double ComputeCFL(void *s,void *m, double dt)
{
  IBCartINS3D   *solver = (IBCartINS3D*)  s;
  MPIVariables  *mpi    = (MPIVariables*) m; 

  int imax    = solver->imax_local;
  int jmax    = solver->jmax_local;
  int kmax    = solver->kmax_local;
  int ghosts  = solver->ghosts;

  int ip = mpi->ip; int iproc = mpi->iproc; int is = mpi->is;
  int jp = mpi->jp; int jproc = mpi->jproc; int js = mpi->js;
  int kp = mpi->kp; int kproc = mpi->kproc; int ks = mpi->ks;

  double max_cfl = 0;
  int i,j,k;
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p1, p2;
        double uc, vc, wc;

        /* calculate cell-centered velocity components */
        p1 = Index1D(i  ,j,k,imax+1,jmax,kmax,ghosts);
        p2 = Index1D(i+1,j,k,imax+1,jmax,kmax,ghosts);
        uc = 0.5 * (solver->iblanku[p1]*solver->u[p1] + solver->iblanku[p2]*solver->u[p2]);
        p1 = Index1D(i,j  ,k,imax,jmax+1,kmax,ghosts);
        p2 = Index1D(i,j+1,k,imax,jmax+1,kmax,ghosts);
        vc = 0.5 * (solver->iblankv[p1]*solver->v[p1] + solver->iblankv[p2]*solver->v[p2]);
        p1 = Index1D(i,j,k  ,imax,jmax,kmax+1,ghosts);
        p2 = Index1D(i,j,k+1,imax,jmax,kmax+1,ghosts);
        wc = 0.5 * (solver->iblankw[p1]*solver->w[p1] + solver->iblankw[p2]*solver->w[p2]);

        /* calculate grid spacings */
        double dx, dy, dz;
        if      ((i == 0) && (ip == 0))             dx = (solver->xg[is+i+1]-solver->xg[is+i]);
        else if ((i == imax-1) && (ip == iproc-1))  dx = (solver->xg[is+i]-solver->xg[is+i-1]);
        else                                        dx = 0.5 * (solver->xg[is+i+1]-solver->xg[is+i-1]);
        if      ((j == 0) && (jp == 0))             dy = (solver->yg[js+j+1]-solver->yg[js+j]);
        else if ((j == jmax-1) && (jp == jproc-1))  dy = (solver->yg[js+j]-solver->yg[js+j-1]);
        else                                        dy = 0.5 * (solver->yg[js+j+1]-solver->yg[js+j-1]);
        if      ((k == 0) && (kp == 0))             dz = (solver->zg[ks+k+1]-solver->zg[ks+k]);
        else if ((k == kmax-1) && (kp == kproc-1))  dz = (solver->zg[ks+k]-solver->zg[ks+k-1]);
        else                                        dz = 0.5 * (solver->zg[ks+k+1]-solver->zg[ks+k-1]);

        double cflx, cfly, cflz;
        cflx = absolute(uc * dt / dx);
        cfly = absolute(vc * dt / dy);
        cflz = absolute(wc * dt / dz);

        if (cflx > max_cfl) max_cfl = cflx;
        if (cfly > max_cfl) max_cfl = cfly;
        if (cflz > max_cfl) max_cfl = cflz;
      }
    }
  }
  return(max_cfl);
}

