#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arrayfunctions.h>
#include <mpivars.h>
#include <mathroutines.h>
#include <ibcartins3d.h>
#include <immersedbody.h>

static int ExtendIBlank2D(char*,int,int,int,int,double*);

int InitializeImmersedBody(void *s,void *m)
{
  IBCartINS3D   *solver = (IBCartINS3D*) s;
  MPIVariables  *mpi    = (MPIVariables*) m;
  int           ierr    = 0;

  ImmersedBody  *ib = (ImmersedBody*) calloc (1,sizeof(ImmersedBody));
  solver->body      = (void*) ib;

  int imax_global = solver->imax_global;
  int jmax_global = solver->jmax_global;
  int kmax_global = solver->kmax_global;
  int imax_local  = solver->imax_local;
  int jmax_local  = solver->jmax_local;
  int kmax_local  = solver->kmax_local;
  int ghosts      = solver->ghosts;

  if (!mpi->rank) {
    /* Root process reads in the STL geometry of the immersed body */
    printf("Reading immersed body data.\n");
    ierr = STLFileRead(solver->stl_filename,&ib->surface,&ib->nfacets);
    if (ierr) return(ierr);
    if (ib->surface)  ib->is_present = 1;
    else              ib->is_present = 0;
  }
  MPIBroadcast_integer(&ib->is_present,1,0);
  MPIBroadcast_integer(&ib->nfacets,1,0);
  if (ib->is_present && mpi->rank)
    /* other processes allocate space to save the immersed body */
    ib->surface = (facet*) calloc (ib->nfacets,sizeof(facet));

  if (ib->is_present) {
    if (!mpi->rank) printf("Number of facets: %10d.\n",ib->nfacets);
    /* Usually, number of facets describing immersed body is  */
    /* much smaller than grid sizes, so let all processors    */
    /* have a copy of the body geometry.                      */
    double *buffer = (double*) calloc (12*ib->nfacets,sizeof(double));
    if (!mpi->rank) {
      int n;
      for (n = 0; n < ib->nfacets; n++) {
				buffer[12*n+0]  = ib->surface[n].x1;
				buffer[12*n+1]  = ib->surface[n].x2;
				buffer[12*n+2]  = ib->surface[n].x3;
				buffer[12*n+3]  = ib->surface[n].y1;
				buffer[12*n+4]  = ib->surface[n].y2;
				buffer[12*n+5]  = ib->surface[n].y3;
				buffer[12*n+6]  = ib->surface[n].z1;
				buffer[12*n+7]  = ib->surface[n].z2;
				buffer[12*n+8]  = ib->surface[n].z3;
				buffer[12*n+9]  = ib->surface[n].nx;
				buffer[12*n+10] = ib->surface[n].ny;
				buffer[12*n+11] = ib->surface[n].nz;
      }
    }
    MPIBroadcast_double(buffer,12*ib->nfacets,0);
    if (mpi->rank) {
      int n;
      for (n = 0; n < ib->nfacets; n++) {
				ib->surface[n].x1 = buffer[12*n+0];
				ib->surface[n].x2 = buffer[12*n+1];
				ib->surface[n].x3 = buffer[12*n+2];
				ib->surface[n].y1 = buffer[12*n+3];
				ib->surface[n].y2 = buffer[12*n+4];
				ib->surface[n].y3 = buffer[12*n+5];
				ib->surface[n].z1 = buffer[12*n+6];
				ib->surface[n].z2 = buffer[12*n+7];
				ib->surface[n].z3 = buffer[12*n+8];
				ib->surface[n].nx = buffer[12*n+9];
				ib->surface[n].ny = buffer[12*n+10];
				ib->surface[n].nz = buffer[12*n+11];
      }
    }
    free(buffer);

    int total, totalu, totalv, totalw; /* variables to find total number of points inside body */
    if (!mpi->rank) printf("Partitioning domain into interior, exterior and immersed boundary.\n");

    int dim[7],limits[6];

    /* Identify the body within the main grid */
    dim[0] = imax_global;
    dim[1] = jmax_global;
    dim[2] = kmax_global;
    dim[3] = imax_local;
    dim[4] = jmax_local;
    dim[5] = kmax_local;
    dim[6] = ghosts;
    limits[0] = mpi->is;  limits[1] = mpi->ie;
    limits[2] = mpi->js;  limits[3] = mpi->je;
    limits[4] = mpi->ks;  limits[5] = mpi->ke;
    solver->nsolid = IBIdentifyBody(ib,&dim[0],&limits[0],solver->xg,solver->yg,solver->zg,solver->iblank);

    /* Identify the body within the staggered u grid */
    dim[0] = imax_global + 1;
    dim[1] = jmax_global;
    dim[2] = kmax_global;
    dim[3] = imax_local  + 1;
    dim[4] = jmax_local;
    dim[5] = kmax_local;
    dim[6] = ghosts;
    limits[0] = mpi->is;  limits[1] = mpi->ie;
    limits[2] = mpi->js;  limits[3] = mpi->je;
    limits[4] = mpi->ks;  limits[5] = mpi->ke;
    limits[1]++;
    solver->nsolidu = IBIdentifyBody(ib,&dim[0],&limits[0],solver->xgu,solver->yg,solver->zg,solver->iblanku);

    /* Identify the body within the staggered v grid */
    dim[0] = imax_global;
    dim[1] = jmax_global + 1;
    dim[2] = kmax_global;
    dim[3] = imax_local;
    dim[4] = jmax_local  + 1;
    dim[5] = kmax_local;
    dim[6] = ghosts;
    limits[0] = mpi->is;  limits[1] = mpi->ie;
    limits[2] = mpi->js;  limits[3] = mpi->je;
    limits[4] = mpi->ks;  limits[5] = mpi->ke;
    limits[3]++;
    solver->nsolidv = IBIdentifyBody(ib,&dim[0],&limits[0],solver->xg,solver->ygv,solver->zg,solver->iblankv);
    
    /* Identify the body within the staggered w grid */
    dim[0] = imax_global;
    dim[1] = jmax_global;
    dim[2] = kmax_global + 1;
    dim[3] = imax_local;
    dim[4] = jmax_local;
    dim[5] = kmax_local  + 1;
    dim[6] = ghosts;
    limits[0] = mpi->is;  limits[1] = mpi->ie;
    limits[2] = mpi->js;  limits[3] = mpi->je;
    limits[4] = mpi->ks;  limits[5] = mpi->ke;
    limits[5]++;
    solver->nsolidw = IBIdentifyBody(ib,&dim[0],&limits[0],solver->xg,solver->yg,solver->zgw,solver->iblankw);

    if (!mpi->rank) printf("Immersed body bounding box: [%f,%f] X [%f,%f] X [%f,%f].\n",
                            ib->xmin,ib->xmax,ib->ymin,ib->ymax,ib->zmin,ib->zmax);

    /* If 2D flow case, iblank arrays need to be extended along the 3rd dimension into the ghost point zones */
    ierr = ExtendIBlank2D(solver->mode2d,imax_local  ,jmax_local  ,kmax_local  ,ghosts,solver->iblank );
    if (ierr) return(ierr);
    ierr = ExtendIBlank2D(solver->mode2d,imax_local+1,jmax_local  ,kmax_local  ,ghosts,solver->iblanku);
    if (ierr) return(ierr);
    ierr = ExtendIBlank2D(solver->mode2d,imax_local  ,jmax_local+1,kmax_local  ,ghosts,solver->iblankv);
    if (ierr) return(ierr);
    ierr = ExtendIBlank2D(solver->mode2d,imax_local  ,jmax_local  ,kmax_local+1,ghosts,solver->iblankw);
    if (ierr) return(ierr);

    total = totalu = totalv = totalw = 0;
    MPISum_integer(&total ,&solver->nsolid ,1);
    MPISum_integer(&totalu,&solver->nsolidu,1);
    MPISum_integer(&totalv,&solver->nsolidv,1);
    MPISum_integer(&totalw,&solver->nsolidw,1);
    if (!mpi->rank) {
      printf("Number of immersed body points on main mesh: %10d\n",total );
      printf("Number of immersed body points on    u mesh: %10d\n",totalu);
      printf("Number of immersed body points on    v mesh: %10d\n",totalv);
      printf("Number of immersed body points on    w mesh: %10d\n",totalw);
    }

    /* exchange iblanking data between processors */
    ierr = MPIExchangeBoundaries(imax_local  ,jmax_local  ,kmax_local  ,ghosts,0,0,0,mpi,solver->iblank );
    if (ierr) return(ierr);
    ierr = MPIExchangeBoundaries(imax_local+1,jmax_local  ,kmax_local  ,ghosts,1,0,0,mpi,solver->iblanku);
    if (ierr) return(ierr);
    ierr = MPIExchangeBoundaries(imax_local  ,jmax_local+1,kmax_local  ,ghosts,0,1,0,mpi,solver->iblankv);
    if (ierr) return(ierr);
    ierr = MPIExchangeBoundaries(imax_local  ,jmax_local  ,kmax_local+1,ghosts,0,0,1,mpi,solver->iblankw);
    if (ierr) return(ierr);

    /* Count number immersed boundary points */
    if (!mpi->rank) printf("Identifying immersed boundary points.\n");
    solver->nboundary  = IBCountBoundaryPoints(imax_local  ,jmax_local  ,kmax_local  ,ghosts,solver->iblank );
    solver->nboundaryu = IBCountBoundaryPoints(imax_local+1,jmax_local  ,kmax_local  ,ghosts,solver->iblanku);
    solver->nboundaryv = IBCountBoundaryPoints(imax_local  ,jmax_local+1,kmax_local  ,ghosts,solver->iblankv);
    solver->nboundaryw = IBCountBoundaryPoints(imax_local  ,jmax_local  ,kmax_local+1,ghosts,solver->iblankw);

    /* Allocate arrays for immersed boundary points on each mesh */
    BoundaryNode *bound,*boundu,*boundv,*boundw;
    bound  = (BoundaryNode*) calloc (solver->nboundary , sizeof(BoundaryNode));
    boundu = (BoundaryNode*) calloc (solver->nboundaryu, sizeof(BoundaryNode));
    boundv = (BoundaryNode*) calloc (solver->nboundaryv, sizeof(BoundaryNode));
    boundw = (BoundaryNode*) calloc (solver->nboundaryw, sizeof(BoundaryNode));
    solver->immersed_boundary  = (void*) bound;
    solver->immersed_boundaryu = (void*) boundu;
    solver->immersed_boundaryv = (void*) boundv;
    solver->immersed_boundaryw = (void*) boundw;

    /* Set immersed boundary indices */
    ierr = IBSetBoundaryIndices(imax_local  ,jmax_local  ,kmax_local  ,ghosts,solver->iblank ,bound );
    if (ierr) return(ierr);
    ierr = IBSetBoundaryIndices(imax_local+1,jmax_local  ,kmax_local  ,ghosts,solver->iblanku,boundu);
    if (ierr) return(ierr);
    ierr = IBSetBoundaryIndices(imax_local  ,jmax_local+1,kmax_local  ,ghosts,solver->iblankv,boundv);
    if (ierr) return(ierr);
    ierr = IBSetBoundaryIndices(imax_local  ,jmax_local  ,kmax_local+1,ghosts,solver->iblankw,boundw);
    if (ierr) return(ierr);

    /* count and print total number of immersed boundary points */
    total = totalu = totalv = totalw = 0;
    MPISum_integer(&total ,&solver->nboundary ,1);
    MPISum_integer(&totalu,&solver->nboundaryu,1);
    MPISum_integer(&totalv,&solver->nboundaryv,1);
    MPISum_integer(&totalw,&solver->nboundaryw,1);
    if (!mpi->rank) {
      printf("Number of immersed boundary points on main mesh: %10d\n",total );
      printf("Number of immersed boundary points on    u mesh: %10d\n",totalu);
      printf("Number of immersed boundary points on    v mesh: %10d\n",totalv);
      printf("Number of immersed boundary points on    w mesh: %10d\n",totalw);
    }

    /* Calculate nearest normals */
    if (!mpi->rank) printf("Finding nearest normals to the boundary nodes.\n");
    int     nb,nf = ib->nfacets;
    void    *boundary;
    double  large_distance = 1.0e6*(solver->xg[solver->imax_global-1]-solver->xg[0]);
    /* main grid */
    nb        = solver->nboundary;
    boundary  = solver->immersed_boundary;
    ierr      = IBNearestNormal(nb,nf,ib->surface,boundary,solver->x,solver->y,solver->z,large_distance);
    if (ierr) return(ierr);
    /* u grid */
    nb        = solver->nboundaryu;
    boundary  = solver->immersed_boundaryu;
    ierr      = IBNearestNormal(nb,nf,ib->surface,boundary,solver->xu,solver->y,solver->z,large_distance);
    if (ierr) return(ierr);
    /* v grid */
    nb        = solver->nboundaryv;
    boundary  = solver->immersed_boundaryv;
    ierr      = IBNearestNormal(nb,nf,ib->surface,boundary,solver->x,solver->yv,solver->z,large_distance);
    if (ierr) return(ierr);
    /* w grid */
    nb        = solver->nboundaryw;
    boundary  = solver->immersed_boundaryw;
    ierr      = IBNearestNormal(nb,nf,ib->surface,boundary,solver->x,solver->y,solver->zw,large_distance);
    if (ierr) return(ierr);

    if (!mpi->rank) printf("Immersed body initializations complete.\n");

  } else {
    if (!mpi->rank)  printf("No immersed body present.\n");
    ierr = SetArrayValue(solver->iblank ,(imax_local+2*ghosts)  *(jmax_local+2*ghosts)  *(kmax_local+2*ghosts)  ,1.0);
    if (ierr) return(ierr);
    ierr = SetArrayValue(solver->iblanku,(imax_local+2*ghosts+1)*(jmax_local+2*ghosts)  *(kmax_local+2*ghosts)  ,1.0);
    if (ierr) return(ierr);
    ierr = SetArrayValue(solver->iblankv,(imax_local+2*ghosts)  *(jmax_local+2*ghosts+1)*(kmax_local+2*ghosts)  ,1.0);
    if (ierr) return(ierr);
    ierr = SetArrayValue(solver->iblankw,(imax_local+2*ghosts)  *(jmax_local+2*ghosts)  *(kmax_local+2*ghosts+1),1.0);
    if (ierr) return(ierr);

    solver->immersed_boundary  = NULL;
    solver->immersed_boundaryu = NULL;
    solver->immersed_boundaryv = NULL;
    solver->immersed_boundaryw = NULL;
  }

  return(0);
}

int ExtendIBlank2D(char *mode2d,int imax,int jmax,int kmax,int ghosts,double *ib)
{
  int i, j, k;
  if (!strcmp(mode2d,"xy")) {
    for (i = 0; i < imax; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = -ghosts; k < 0; k++) {
          int pb  = Index1D(i,j,k,imax,jmax,kmax,ghosts);
          int pi  = Index1D(i,j,0,imax,jmax,kmax,ghosts);
          ib[pb] = ib[pi];
        }
      }
    }
    for (i = 0; i < imax; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = kmax; k < kmax+ghosts; k++) {
          int pb  = Index1D(i,j,k     ,imax,jmax,kmax,ghosts);
          int pi  = Index1D(i,j,kmax-1,imax,jmax,kmax,ghosts);
          ib[pb] = ib[pi];
        }
      }
    }
  } else if (!strcmp(mode2d,"yz")) {
    for (i = -ghosts; i < 0; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = 0; k < kmax; k++) {
          int pb = Index1D(i,j,k,imax,jmax,kmax,ghosts);
          int pi = Index1D(0,j,k,imax,jmax,kmax,ghosts);
          ib[pb] = ib[pi];
        }
      }
    }
    for (i = imax; i < imax+ghosts; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = 0; k < kmax; k++) {
          int pb = Index1D(i     ,j,k,imax,jmax,kmax,ghosts);
          int pi = Index1D(imax-1,j,k,imax,jmax,kmax,ghosts);
          ib[pb] = ib[pi];
        }
      }
    }
  } else if (!strcmp(mode2d,"xz")) {
    for (i = 0; i < imax; i++) {
      for (j = -ghosts; j < 0; j++) {
        for (k = 0; k < kmax; k++) {
          int pb = Index1D(i,j,k,imax,jmax,kmax,ghosts);
          int pi = Index1D(i,0,k,imax,jmax,kmax,ghosts);
          ib[pb] = ib[pi];
        }
      }
    }
    for (i = 0; i < imax; i++) {
      for (j = jmax; j < jmax+ghosts; j++) {
        for (k = 0; k < kmax; k++) {
          int pb = Index1D(i,j     ,k,imax,jmax,kmax,ghosts);
          int pi = Index1D(i,jmax-1,k,imax,jmax,kmax,ghosts);
          ib[pb] = ib[pi];
        }
      }
    }
  }
  return(0);
}
