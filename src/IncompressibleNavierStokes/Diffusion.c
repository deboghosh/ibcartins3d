#include <arrayfunctions.h>
#include <mpivars.h>
#include <ibcartins3d.h>

static int Laplacian (double*,double*,void*,int*,double*,double*,double*,double);

int Diffusion(void *s,void *m,double *u,double *v,double *w,
              double *rhs_u,double *rhs_v,double *rhs_w)
{
  MPIVariables  *mpi    = (MPIVariables*) m;
  IBCartINS3D   *solver = (IBCartINS3D*) s;
  int           ierr    = 0;

  double *x, *y, *z;
  int    dim[4];
  double coeff = solver->mu;

  /* u velocity */
  dim[0] = solver->imax_local+1;  x = solver->xgu;
  dim[1] = solver->jmax_local;    y = solver->yg;
  dim[2] = solver->kmax_local;    z = solver->zg;
  dim[3] = solver->ghosts;
  ierr = Laplacian(rhs_u,u,mpi,&dim[0],x,y,z,coeff);
  if (ierr) return(ierr);

  /* v velocity */
  dim[0] = solver->imax_local;    x = solver->xg;
  dim[1] = solver->jmax_local+1;  y = solver->ygv;
  dim[2] = solver->kmax_local;    z = solver->zg;
  dim[3] = solver->ghosts;
  ierr = Laplacian(rhs_v,v,mpi,&dim[0],x,y,z,coeff);
  if (ierr) return(ierr);

  /* w velocity */
  dim[0] = solver->imax_local;    x = solver->xg;
  dim[1] = solver->jmax_local;    y = solver->yg;
  dim[2] = solver->kmax_local+1;  z = solver->zgw;
  dim[3] = solver->ghosts;
  ierr = Laplacian(rhs_w,w,mpi,&dim[0],x,y,z,coeff);
  if (ierr) return(ierr);

  return(0);
}

int Laplacian(double *rhs,double *q,void *m,int *dim,
              double *x,double *y,double *z,double coeff)
{
  MPIVariables  *mpi    = (MPIVariables*) m;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  int ip = mpi->ip; int iproc = mpi->iproc; int is = mpi->is;
  int jp = mpi->jp; int jproc = mpi->jproc; int js = mpi->js;
  int kp = mpi->kp; int kproc = mpi->kproc; int ks = mpi->ks;

  int i,j,k;
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        double Aw,Ae,As,An,Ab,At,Ac;
        if ((k == 0) && (kp == 0)) {
          Ab = 1.0 / ((z[ks+k+1]-z[ks+k]) * (z[ks+k+1]-z[ks+k]));
				  At = 1.0 / ((z[ks+k+1]-z[ks+k]) * (z[ks+k+1]-z[ks+k]));
        } else if ((k == kmax-1) && (kp == kproc-1)) {
          Ab = 1.0 / ((z[ks+k]-z[ks+k-1]) * (z[ks+k]-z[ks+k-1]));
				  At = 1.0 / ((z[ks+k]-z[ks+k-1]) * (z[ks+k]-z[ks+k-1]));
        } else {
          Ab = 2.0 / ((z[ks+k+1]-z[ks+k-1]) * (z[ks+k]-z[ks+k-1]));
				  At = 2.0 / ((z[ks+k+1]-z[ks+k-1]) * (z[ks+k+1]-z[ks+k]));
        }
        if ((j == 0) && (jp == 0)) {
          As = 1.0 / ((y[js+j+1]-y[js+j]) * (y[js+j+1]-y[js+j]));
          An = 1.0 / ((y[js+j+1]-y[js+j]) * (y[js+j+1]-y[js+j]));
        } else if ((j == jmax-1) && (jp == jproc-1)) {
          As = 1.0 / ((y[js+j]-y[js+j-1]) * (y[js+j]-y[js+j-1]));
          An = 1.0 / ((y[js+j]-y[js+j-1]) * (y[js+j]-y[js+j-1]));
        } else {
          As = 2.0 / ((y[js+j+1]-y[js+j-1]) * (y[js+j]-y[js+j-1]));
          An = 2.0 / ((y[js+j+1]-y[js+j-1]) * (y[js+j+1]-y[js+j]));
        }
        if ((i == 0) && (ip == 0)) {
          Aw = 1.0 / ((x[is+i+1]-x[is+i]) * (x[is+i+1]-x[is+i]));
          Ae = 1.0 / ((x[is+i+1]-x[is+i]) * (x[is+i+1]-x[is+i]));
        } else if ((i == imax-1) && (ip == iproc-1)) {
          Aw = 1.0 / ((x[is+i]-x[is+i-1]) * (x[is+i]-x[is+i-1]));
          Ae = 1.0 / ((x[is+i]-x[is+i-1]) * (x[is+i]-x[is+i-1]));
        } else {
          Aw = 2.0 / ((x[is+i+1]-x[is+i-1]) * (x[is+i]-x[is+i-1]));
          Ae = 2.0 / ((x[is+i+1]-x[is+i-1]) * (x[is+i+1]-x[is+i]));
        }
				Ac = -(Ab+As+Aw+Ae+An+At);

        int pC = Index1D(i  ,j  ,k  ,imax,jmax,kmax,ghosts);
        int pW = Index1D(i-1,j  ,k  ,imax,jmax,kmax,ghosts);
        int pE = Index1D(i+1,j  ,k  ,imax,jmax,kmax,ghosts);
        int pS = Index1D(i  ,j-1,k  ,imax,jmax,kmax,ghosts);
        int pN = Index1D(i  ,j+1,k  ,imax,jmax,kmax,ghosts);
        int pB = Index1D(i  ,j  ,k-1,imax,jmax,kmax,ghosts);
        int pT = Index1D(i  ,j  ,k+1,imax,jmax,kmax,ghosts);
        int p  = Index1D(i  ,j  ,k  ,imax,jmax,kmax,0);

        double diff = 0;
        diff += Ab * q[pB];
        diff += At * q[pT];
        diff += As * q[pS];
        diff += An * q[pN];
        diff += Aw * q[pW];
        diff += Ae * q[pE];
        diff += Ac * q[pC];

        rhs[p] += coeff * diff;

      }
    }
  }
  return(0);
}
