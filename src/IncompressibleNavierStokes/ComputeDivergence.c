#include <arrayfunctions.h>
#include <mpivars.h>
#include <ibcartins3d.h>

int ComputeDivergence(double *divergence,void *s,void *m,double *norm)
{
  IBCartINS3D   *solver = (IBCartINS3D*) s;
  int           ierr    = 0;

  int imax    = solver->imax_local;
  int jmax    = solver->jmax_local;
  int kmax    = solver->kmax_local;
  int ghosts  = solver->ghosts;

  double *u  = solver->u;
  double *v  = solver->v;
  double *w  = solver->w;
  double *xu = solver->xu;
  double *yv = solver->yv;
  double *zw = solver->zw;

  int i,j,k;
  double div_norm = 0;
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p  = Index1D(i  ,j  ,k  ,imax  ,jmax  ,kmax  ,0     );
        int pW = Index1D(i  ,j  ,k  ,imax+1,jmax  ,kmax  ,ghosts);
        int pE = Index1D(i+1,j  ,k  ,imax+1,jmax  ,kmax  ,ghosts);
        int pS = Index1D(i  ,j  ,k  ,imax  ,jmax+1,kmax  ,ghosts);
        int pN = Index1D(i  ,j+1,k  ,imax  ,jmax+1,kmax  ,ghosts);
        int pB = Index1D(i  ,j  ,k  ,imax  ,jmax  ,kmax+1,ghosts);
        int pT = Index1D(i  ,j  ,k+1,imax  ,jmax  ,kmax+1,ghosts);

        double u_x = (u[pE]-u[pW]) / (xu[i+1]-xu[i]);
        double v_y = (v[pN]-v[pS]) / (yv[j+1]-yv[j]);
        double w_z = (w[pT]-w[pB]) / (zw[k+1]-zw[k]);

        double div    =  u_x + v_y + w_z;
        divergence[p] =  div;
        div_norm      += (div * div);
      }
    }
  }

  /* sum over all processes */
  int total_npoints, npoints = imax*jmax*kmax;
  ierr = MPISum_double  (norm          ,&div_norm,1); if (ierr) return(ierr);
  ierr = MPISum_integer (&total_npoints,&npoints ,1); if (ierr) return(ierr);
  *norm /= total_npoints;

  return(0);
}
