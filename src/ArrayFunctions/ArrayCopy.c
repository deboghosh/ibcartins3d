#include <arrayfunctions.h>

int ArrayCopy(double *source,double *dest,int size)
{
  if ((!source) || (!dest)) return(1);

  int i;
  for (i = 0; i < size; i++) dest[i] = source[i];
  return(0);
}
