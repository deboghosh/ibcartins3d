#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <boundaryconditions.h>

int BCInitialize(void *b, int *dim)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  int imax    = dim[0]+1;
  int jmax    = dim[1]+1;
  int kmax    = dim[2]+1;
  int ghosts  = dim[3];

  if (!strcmp(boundary->bctype,_NOSLIP_)) {
    if (!strcmp(boundary->face,"imin")) {
      boundary->BCFunctionVelocity = BCVelocitySolid_imin;
      boundary->BCFunctionPressure = BCPressureSolid_imin;
    } else if (!strcmp(boundary->face,"imax")) {
      boundary->BCFunctionVelocity = BCVelocitySolid_imax;
      boundary->BCFunctionPressure = BCPressureSolid_imax;
    } else if (!strcmp(boundary->face,"jmin")) {
      boundary->BCFunctionVelocity = BCVelocitySolid_jmin;
      boundary->BCFunctionPressure = BCPressureSolid_jmin;
    } else if (!strcmp(boundary->face,"jmax")) {
      boundary->BCFunctionVelocity = BCVelocitySolid_jmax;
      boundary->BCFunctionPressure = BCPressureSolid_jmax;
    } else if (!strcmp(boundary->face,"kmin")) {
      boundary->BCFunctionVelocity = BCVelocitySolid_kmin;
      boundary->BCFunctionPressure = BCPressureSolid_kmin;
    } else if (!strcmp(boundary->face,"kmax")) {
      boundary->BCFunctionVelocity = BCVelocitySolid_kmax;
      boundary->BCFunctionPressure = BCPressureSolid_kmax;
    } else {
      fprintf(stderr,"Error in InitializeBoundaries(): %s is not a valid face type.\n",
              boundary->face);
      return(1);
    }
  } else if (!strcmp(boundary->bctype,_PERIODIC_)) {
    if (!strcmp(boundary->face,"imin")) {
      boundary->BCFunctionVelocity = BCVelocityPeriodic_imin;
      boundary->BCFunctionPressure = BCPressurePeriodic_imin;
      boundary->buffer = (double*) calloc (ghosts*jmax*kmax,sizeof(double));
      boundary->bufdim[0] = ghosts;
      boundary->bufdim[1] = jmax;
      boundary->bufdim[2] = kmax;
    } else if (!strcmp(boundary->face,"imax")) {
      boundary->BCFunctionVelocity = BCVelocityPeriodic_imax;
      boundary->BCFunctionPressure = BCPressurePeriodic_imax;
      boundary->buffer = (double*) calloc (ghosts*jmax*kmax,sizeof(double));
      boundary->bufdim[0] = ghosts;
      boundary->bufdim[1] = jmax;
      boundary->bufdim[2] = kmax;
    } else if (!strcmp(boundary->face,"jmin")) {
      boundary->BCFunctionVelocity = BCVelocityPeriodic_jmin;
      boundary->BCFunctionPressure = BCPressurePeriodic_jmin;
      boundary->buffer = (double*) calloc (ghosts*imax*kmax,sizeof(double));
      boundary->bufdim[0] = imax;
      boundary->bufdim[1] = ghosts;
      boundary->bufdim[2] = kmax;
    } else if (!strcmp(boundary->face,"jmax")) {
      boundary->BCFunctionVelocity = BCVelocityPeriodic_jmax;
      boundary->BCFunctionPressure = BCPressurePeriodic_jmax;
      boundary->buffer = (double*) calloc (ghosts*imax*kmax,sizeof(double));
      boundary->bufdim[0] = imax;
      boundary->bufdim[1] = ghosts;
      boundary->bufdim[2] = kmax;
    } else if (!strcmp(boundary->face,"kmin")) {
      boundary->BCFunctionVelocity = BCVelocityPeriodic_kmin;
      boundary->BCFunctionPressure = BCPressurePeriodic_kmin;
      boundary->buffer = (double*) calloc (ghosts*jmax*imax,sizeof(double));
      boundary->bufdim[0] = imax;
      boundary->bufdim[1] = jmax;
      boundary->bufdim[2] = ghosts;
    } else if (!strcmp(boundary->face,"kmax")) {
      boundary->BCFunctionVelocity = BCVelocityPeriodic_kmax;
      boundary->BCFunctionPressure = BCPressurePeriodic_kmax;
      boundary->buffer = (double*) calloc (ghosts*jmax*imax,sizeof(double));
      boundary->bufdim[0] = imax;
      boundary->bufdim[1] = jmax;
      boundary->bufdim[2] = ghosts;
    } else {
      fprintf(stderr,"Error in InitializeBoundaries(): \"%s\" is not a valid face type.\n",
              boundary->face);
      return(1);
    }
//} else if (!strcmp(boundary->bctype,_SYMMETRIC_)) {
  } else if (!strcmp(boundary->bctype,_EXTRAPOLATE_)) {
    if (!strcmp(boundary->face,"imin")) {
      boundary->BCFunctionVelocity = BCVelocityExtrapolate_imin;
      boundary->BCFunctionPressure = BCPressureExtrapolate_imin;
    } else if (!strcmp(boundary->face,"imax")) {
      boundary->BCFunctionVelocity = BCVelocityExtrapolate_imax;
      boundary->BCFunctionPressure = BCPressureExtrapolate_imax;
    } else if (!strcmp(boundary->face,"jmin")) {
      boundary->BCFunctionVelocity = BCVelocityExtrapolate_jmin;
      boundary->BCFunctionPressure = BCPressureExtrapolate_jmin;
    } else if (!strcmp(boundary->face,"jmax")) {
      boundary->BCFunctionVelocity = BCVelocityExtrapolate_jmax;
      boundary->BCFunctionPressure = BCPressureExtrapolate_jmax;
    } else if (!strcmp(boundary->face,"kmin")) {
      boundary->BCFunctionVelocity = BCVelocityExtrapolate_kmin;
      boundary->BCFunctionPressure = BCPressureExtrapolate_kmin;
    } else if (!strcmp(boundary->face,"kmax")) {
      boundary->BCFunctionVelocity = BCVelocityExtrapolate_kmax;
      boundary->BCFunctionPressure = BCPressureExtrapolate_kmax;
    } else {
      fprintf(stderr,"Error in InitializeBoundaries(): %s is not a valid face type.\n",
              boundary->face);
      return(1);
    }
  } else if (!strcmp(boundary->bctype,_FREESTREAM_)) {
    if (!strcmp(boundary->face,"imin")) {
      boundary->BCFunctionVelocity = BCVelocityFreestream_imin;
      boundary->BCFunctionPressure = BCPressureFreestream_imin;
    } else if (!strcmp(boundary->face,"imax")) {
      boundary->BCFunctionVelocity = BCVelocityFreestream_imax;
      boundary->BCFunctionPressure = BCPressureFreestream_imax;
    } else if (!strcmp(boundary->face,"jmin")) {
      boundary->BCFunctionVelocity = BCVelocityFreestream_jmin;
      boundary->BCFunctionPressure = BCPressureFreestream_jmin;
    } else if (!strcmp(boundary->face,"jmax")) {
      boundary->BCFunctionVelocity = BCVelocityFreestream_jmax;
      boundary->BCFunctionPressure = BCPressureFreestream_jmax;
    } else if (!strcmp(boundary->face,"kmin")) {
      boundary->BCFunctionVelocity = BCVelocityFreestream_kmin;
      boundary->BCFunctionPressure = BCPressureFreestream_kmin;
    } else if (!strcmp(boundary->face,"kmax")) {
      boundary->BCFunctionVelocity = BCVelocityFreestream_kmax;
      boundary->BCFunctionPressure = BCPressureFreestream_kmax;
    } else {
      fprintf(stderr,"Error in InitializeBoundaries(): %s is not a valid face type.\n",
              boundary->face);
      return(1);
    }
  } else {
    fprintf(stderr,"Error in InitializeBoundaries(): \"%s\" is not a supported boundary condition.\n",
            boundary->bctype);
    return(1);
  }

  return(0);
}
