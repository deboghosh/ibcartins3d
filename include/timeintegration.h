typedef struct time_integration_variables {
  int     iter;     /* iteration number           */
  int     n_iter;   /* Total number of iterations */
  double  waqt;     /* time                       */
  double  dt;       /* time step                  */
  double  norm;     /* norm of the solution       */
  double  max_cfl;  /* max CFL for a time step    */

  void    *solver;  /* solver object              */
  double  *u,*v,*w; /* arrays to store solution   */

  void*   *ResidualFile;
  void*   *IBForceFile;
} TimeIntegration;

/* Time Integration Functions */
int ForwardEuler  (void*,void*,void*);
int LowStorageRK3 (void*,void*,void*);
